<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

# EDDIE Project

Welcome to the EDDIE, part of the Oniro\* Project, main git repository!
Oniro is an Eclipse Foundation project focused on the development of a
distributed open source operating system for consumer devices.

*\*Oniro is a trademark of Eclipse Foundation.*

# About

The **eddie** repository is home of a middleware in the Oniro Project to enable
distributed intelligence on Oniro devices.

The objective of the EDDIE project is to design
a middleware running on top of the operating system of the various Oniro
devices. Capable of giving to the applications and services an global view
on a unified pool of virtual resources that can be exploited for an efficient
execution of any application or service required by the user.

## Contributing

See the `CONTRIBUTING.md` file.

## License

See the `LICENSES` subdirectory.

## Security

See the `SECURITY.md` file.

## Build instructions
- If you want to run EDDIE on your local machine build the CMake project, then you can run the `eddie-server` and `eddie-client` binaries.
```console
$ mkdir build && cd build
$ cmake ..
$ make
```

- If you want to build the Oniro Blueprint for the EDDIE project, and run on an Oniro supported target: follow the instructions in the [EDDIE blueprint git repository](https://booting.oniroproject.org/distro/blueprints/eddie/meta-oniro-blueprints-eddie).