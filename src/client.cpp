/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */

#include <thread>
#include <condition_variable>

#include "CoapClient.h"

#define COAP_IP4_MULTICAST "224.0.1.187"

int main() {
    int key = 9999;
    coap_set_log_level(LOG_CRIT);
    CoapClient client;
    client.initClient();
    std::string ip = "0.0.0.0";

    auto f = [&client, &key]() {
        while (key) {
            coap_io_process(client.get_context(), COAP_IO_WAIT * 1000);
            key--;
        }
    };

    std::thread t(f);

    auto token = client.send_get_request_non_confirmable(".well-known/core",
                                                         nullptr,
                                                         nullptr,
                                                         0);

    auto f1 = [&client, ip, token]() {
        std::unique_lock<decltype(client.client_mutex)> lock(client.client_mutex);
        auto timeout = std::chrono::system_clock::now() + std::chrono::seconds(COAP_DEFAULT_LEISURE * 2);
        client.client_condition.wait_until(lock, timeout);

        auto answers = CoapClient::multicast_messages->get(token);

        auto pdu = answers.front();

        coap_show_pdu(LOG_EMERG, pdu);

        // extract and set ip
    };

    auto id1 = client.send_get_request(ip.c_str(), "5683", "lamp/brightness", nullptr, nullptr, 0);

    auto f2 = [&client, ip, id1]() {
        std::unique_lock<decltype(client.client_mutex)> lock(client.client_mutex);
        auto timeout = std::chrono::system_clock::now() + std::chrono::seconds(COAP_DEFAULT_LEISURE * 2);

        auto answer = CoapClient::messages->get(id1);

        while (answer == nullptr) {
            client.client_condition.wait_until(lock, timeout);
            answer = CoapClient::messages->get(id1);
        }

        coap_show_pdu(LOG_EMERG, answer);
    };

    auto id2 = client.send_put_request(ip.c_str(), "5683", "lamp/brightness", nullptr, "1");

    auto f3 = [&client, ip, id2]() {
        std::unique_lock<decltype(client.client_mutex)> lock(client.client_mutex);
        auto timeout = std::chrono::system_clock::now() + std::chrono::seconds(COAP_DEFAULT_LEISURE * 2);

        auto answer = CoapClient::messages->get(id2);

        while (answer == nullptr) {
            client.client_condition.wait_until(lock, timeout);
            answer = CoapClient::messages->get(id2);
        }

        coap_show_pdu(LOG_EMERG, answer);
    };

    auto id3 = client.send_get_request(ip.c_str(), "5683", "lamp/brightness", nullptr, nullptr, 0);

    auto f4 = [&client, ip, id3]() {
        std::unique_lock<decltype(client.client_mutex)> lock(client.client_mutex);
        auto timeout = std::chrono::system_clock::now() + std::chrono::seconds(COAP_DEFAULT_LEISURE * 2);

        auto answer = CoapClient::messages->get(id3);

        while (answer == nullptr) {
            client.client_condition.wait_until(lock, timeout);
            answer = CoapClient::messages->get(id3);
        }

        coap_show_pdu(LOG_EMERG, answer);
    };

    std::thread t1(f1);
    t1.join();

    std::thread t2(f2);
    t2.join();

    std::thread t3(f3);
    t3.join();

    std::thread t4(f4);
    t4.join();
}
