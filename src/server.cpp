/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */

#include <string>
#include "CoapServer.h"

int main() {
    // create server with local resource directory
//    coap_set_log_level(LOG_INFO);
    std::string local_vm_ip = "192.168.159.128";
    coap_address_t *dst;

    std::string attr_list_common = R"(addr=""&port="5683")";
    CoapServer server = CoapServer("0.0.0.0",
                                   "5683",
                                   1);

    std::string attr_list_ct = attr_list_common + "&rt=\"actuator\"";

    server.add_resource("lamp", 0, attr_list_common.c_str());
    server.add_resource("lamp/brightness", 0, attr_list_ct.c_str());

    server.run();
}
