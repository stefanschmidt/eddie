/*
 * Some initial code was taken from the libcoap examples licensed under
 * BSD-2-clause (see LICENSES/BSD-2-clause.txt file)
 * Copyright (c) 2010--2021, Olaf Bergmann and others
 * 
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */

#include <netdb.h>
#include <cstdio>
#include <thread>
#include <string>
#include <regex>

#include <coap3/coap.h>
#include <arpa/inet.h>
#include <unistd.h>

#define RD_ROOT_STR "rd\0"
#define RD_ROOT_SIZE 2
#define LOCALHOST "127.0.0.1"

#define min(a, b) ((a) < (b) ? (a) : (b))

//TODO: check free() function calls

static void resource_rd_delete(void *ptr) {
    if (ptr) {
        coap_free(ptr);
    }
}

int resolve_address(const char *host, const char *service, coap_address_t *dst) {
    struct addrinfo *res, *aInfo;
    struct addrinfo hints{};
    int error, len = -1;

    memset(&hints, 0, sizeof(hints));
    memset(dst, 0, sizeof(*dst));
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_family = AF_UNSPEC;

    error = getaddrinfo(host, service, &hints, &res);

    if (error != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(error));
        return error;
    }

    for (aInfo = res; aInfo != nullptr; aInfo = aInfo->ai_next) {
        switch (aInfo->ai_family) {
            case AF_INET6:
            case AF_INET:
                len = dst->size = aInfo->ai_addrlen;
                memcpy(&dst->addr.sin6, aInfo->ai_addr, dst->size);
                freeaddrinfo(res);
                return len;
            default:;
        }
    }

    freeaddrinfo(res);
    return len;
}

static int
parse_param(const uint8_t *search, size_t search_len, unsigned char *data, size_t data_len, coap_string_t *result) {
    if (result)
        memset(result, 0, sizeof(coap_string_t));

    if (!search_len)
        return 0;

    while (search_len <= data_len) {

        /* handle parameter if found */
        if (memcmp(search, data, search_len) == 0) {
            data += search_len;
            data_len -= search_len;

            /* key is only valid if we are at end of string or delimiter follows */
            if (!data_len || *data == '=' || *data == '&') {
                while (data_len && *data != '=') {
                    ++data;
                    --data_len;
                }

                if (data_len > 1 && result) {
                    /* value begins after '=' */

                    result->s = ++data;
                    while (--data_len && *data != '&') {
                        ++data;
                        result->length++;
                    }
                }

                return 1;
            }
        }

        /* if not, proceed to next */
        while (--data_len && *data++ != '&');
    }

    return 0;
}

static void add_source_address(coap_resource_t *resource, const coap_address_t *peer) {
#define BUFSIZE 64
    char *buf;
    size_t n = 1;
    coap_str_const_t attr_val;

    buf = (char *) coap_malloc(BUFSIZE);
    if (!buf)
        return;

    buf[0] = '"';

    switch (peer->addr.sa.sa_family) {
        case AF_INET:
            n += snprintf(buf + n, BUFSIZE - n, "%s", inet_ntoa(peer->addr.sin.sin_addr));

            if (peer->addr.sin.sin_port != htons(COAP_DEFAULT_PORT)) {
                n += snprintf(buf + n, BUFSIZE - n, ":%d", peer->addr.sin.sin_port);
            }
            break;
        case AF_INET6:
            n += snprintf(buf + n, BUFSIZE - n,
                          "[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]",
                          peer->addr.sin6.sin6_addr.s6_addr[0],
                          peer->addr.sin6.sin6_addr.s6_addr[1],
                          peer->addr.sin6.sin6_addr.s6_addr[2],
                          peer->addr.sin6.sin6_addr.s6_addr[3],
                          peer->addr.sin6.sin6_addr.s6_addr[4],
                          peer->addr.sin6.sin6_addr.s6_addr[5],
                          peer->addr.sin6.sin6_addr.s6_addr[6],
                          peer->addr.sin6.sin6_addr.s6_addr[7],
                          peer->addr.sin6.sin6_addr.s6_addr[8],
                          peer->addr.sin6.sin6_addr.s6_addr[9],
                          peer->addr.sin6.sin6_addr.s6_addr[10],
                          peer->addr.sin6.sin6_addr.s6_addr[11],
                          peer->addr.sin6.sin6_addr.s6_addr[12],
                          peer->addr.sin6.sin6_addr.s6_addr[13],
                          peer->addr.sin6.sin6_addr.s6_addr[14],
                          peer->addr.sin6.sin6_addr.s6_addr[15]);

            if (peer->addr.sin6.sin6_port != htons(COAP_DEFAULT_PORT)) {
                n += snprintf(buf + n, BUFSIZE - n, ":%d", peer->addr.sin6.sin6_port);
            }
            break;
        default:
            break;
    }

    if (n < BUFSIZE) {
        buf[n++] = '"';
    }

    attr_val.s = (const uint8_t *) buf;
    attr_val.length = n;
    coap_add_attr(resource,
                  coap_make_str_const("A"),
                  &attr_val,
                  0);
    coap_free(buf);
#undef BUFSIZE
}

/**
 * Split a string in different substrings, given the delimiter character.
 *
 * @param s             the string to analyze
 * @param delimiter     the delimiter character of every substring
 */
std::vector<std::string> split(const std::string &s, char delimiter) {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);

    while (std::getline(tokenStream, token, delimiter)) {
        tokens.push_back(token);
    }

    return tokens;
}

// RESOURCES HANDLERS
/**
 * Handler for GET requests of resources registered in the Registry Directory
 * @param resource  the resource targeted by the GET request
 * @param response  the answer to the GET request; assumed that the resource is present in the RD
 */
static void hnd_get_resource(coap_resource_t *resource,
                             coap_session_t *session COAP_UNUSED,
                             const coap_pdu_t *request COAP_UNUSED,
                             const coap_string_t *query COAP_UNUSED,
                             coap_pdu_t *response) {
    coap_attr_t *etag;
    unsigned char buf[3];

    unsigned char print_buf[1024];
    auto *size = static_cast<size_t *>(malloc(sizeof(size_t)));
    *size = 1024;
    auto *offset = static_cast<size_t *>(malloc(sizeof(size_t)));
    *offset = 0;

    //TODO: what if the string is bigger than the buffer?

    coap_pdu_set_code(response, COAP_RESPONSE_CODE_CONTENT);
    coap_add_option(response,
                    COAP_OPTION_CONTENT_TYPE,
                    coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_LINK_FORMAT),
                    buf);

    coap_print_link(resource, print_buf, size, offset);
    print_buf[*size] = '\0';

    etag = coap_find_attr(resource, coap_make_str_const("etag"));

    if (etag) {
        const char *value = (const char *) coap_attr_get_value(etag);
        if (strlen(value)) {
            coap_add_option(response,
                            COAP_OPTION_ETAG,
                            strlen(value),
                            (const u_int8_t *) value);
        }
    }

    coap_add_data(response, *size, print_buf);
}

/**
 * Handler for DELETE requests of resources registered in the Registry Directory
 * @param resource  the resource targeted by the DELETE request
 * @param response  2.05 if the resource is deleted, 4.04 if the resource was not found
 */
static void hnd_delete_resource(coap_resource_t *resource,
                                coap_session_t *session,
                                const coap_pdu_t *request COAP_UNUSED,
                                const coap_string_t *query COAP_UNUSED,
                                coap_pdu_t *response) {
    coap_context_t *context;
    int result;

    context = coap_session_get_context(session);
    result = coap_delete_resource(context, resource);

    if (result) {
        coap_pdu_set_code(response, COAP_RESPONSE_CODE_DELETED);
    } else {
        coap_pdu_set_code(response, COAP_RESPONSE_CODE_NOT_FOUND);
    }
}

/**
 * Handler for PUT requests: update the state of the specified resource
 * @param query     the query string (format: ?key1=value1&key2=value2) that will be parsed
 * @param response  the answer to the PUT request TODO: spiegare i codici
 */
static void hnd_put_resource(coap_resource_t *resource COAP_UNUSED,
                             coap_session_t *session COAP_UNUSED,
                             const coap_pdu_t *request COAP_UNUSED,
                             const coap_string_t *query,
                             coap_pdu_t *response) {

    // TODO: insert code to prevent memory protection

    const unsigned char *uri_path;
    coap_attr_t *resource_location;
    coap_address_t address{};
    std::string query_str((char *) query->s);
    std::stringstream string_stream;

    std::vector<std::string> tokens = split(query_str, '&');
    std::vector<std::vector<std::string>> query_a;
    query_a.reserve(tokens.size());

    for (const auto &a: tokens) {
        query_a.push_back(split(a, '='));
    }

    for (auto a: query_a) {
        const char *key = a[0].data();
        const char *value = a[1].data();

        coap_attr_t *attr = coap_find_attr(resource, coap_make_str_const(key));

        if (attr) {
            coap_attr_get_value(attr)->s = (const uint8_t *) value;
        } else {
            coap_add_attr(resource,
                          coap_make_str_const(key),
                          coap_make_str_const(value),
                          0);
        }
    }

    coap_pdu_set_code(response, COAP_RESPONSE_CODE_CHANGED);
}

// RESOURCE DIRECTORY HANDLERS
/**
 * Handler for GET requests on rd
 * @param response The response crafted by the Registry Directory
 */
static void hnd_get_rd(coap_resource_t *resource COAP_UNUSED,
                       coap_session_t *session COAP_UNUSED,
                       const coap_pdu_t *request COAP_UNUSED,
                       const coap_string_t *query COAP_UNUSED,
                       coap_pdu_t *response) {
    unsigned char buf[3];

    coap_pdu_set_code(response, COAP_RESPONSE_CODE_CONTENT);

    coap_add_option(response,
                    COAP_OPTION_CONTENT_TYPE,
                    coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_LINK_FORMAT),
                    buf);

    coap_add_option(response,
                    COAP_OPTION_MAXAGE,
                    coap_encode_var_safe(buf, sizeof(buf), 0x2ffff),
                    buf);
}

/**
 * Handler for the POST request on rd
 * @param session   Session of the Registry Directory
 * @param request   Request to be served
 * @param response  Response to the currently served request
 */
static void hnd_post_rd(coap_resource_t *resource COAP_UNUSED,
                        coap_session_t *session,
                        const coap_pdu_t *request,
                        const coap_string_t *query,
                        coap_pdu_t *response) {
    // query->s contains p1=v1&p2=v2&p3=v3&...


    coap_resource_t *r;
    int loc_const = 68;
    unsigned char *loc, *buf;
    coap_string_t h = {0, nullptr}, ins = {0, nullptr}, rt = {0, nullptr}, lt = {0, nullptr};
    size_t loc_size;
    coap_str_const_t attr_val;
    coap_str_const_t resource_val;

    loc = (unsigned char *) coap_malloc(loc_const);
    if (!loc) {
        coap_pdu_set_code(response, COAP_RESPONSE_CODE_INTERNAL_ERROR);
        return;
    }

    strncpy((char *) loc, RD_ROOT_STR, RD_ROOT_SIZE);
    loc_size = RD_ROOT_SIZE;
    loc[loc_size++] = '/';

    if (query) {
        // TODO: parse query string
    }

    if (h.length) { // client sent a name for the resource
        memcpy(loc + loc_size, h.s, min(h.length, loc_const - loc_size - 1));
        loc_size += min(h.length, loc_const - loc_size - 1);

        if (ins.length && loc_size > 1) {
            loc[loc_size++] = '-';
            memcpy((char *) (loc + loc_size), ins.s, min(ins.length, loc_const - loc_size - 1));
            loc_size += min(ins.length, loc_const - loc_size - 1);
        }
    } else {      /* generate node identifier */
        loc_size +=
                snprintf((char *) (loc + loc_size), loc_const - loc_size - 1,
                         "%x", coap_pdu_get_mid(request));

        if (loc_size > 1) {
            if (ins.length) {
                loc[loc_size++] = '-';
                memcpy((char *) (loc + loc_size),
                       ins.s,
                       min(ins.length, loc_const - loc_size - 1));
                loc_size += min(ins.length, loc_const - loc_size - 1);
            } else {
                coap_tick_t now;
                coap_ticks(&now);

                loc_size += snprintf((char *) (loc + loc_size),
                                     loc_const - loc_size - 1,
                                     "-%x",
                                     (unsigned int) (now & (unsigned int) -1));
            }
        }
    }

    // FIXME: check expiration with lt

    // assign handlers for the related resource
    resource_val.s = loc;
    resource_val.length = loc_size;
    r = coap_resource_init(&resource_val, 0);
    coap_register_handler(r, COAP_REQUEST_GET, hnd_get_resource);
    coap_register_handler(r, COAP_REQUEST_PUT, hnd_put_resource);
    coap_register_handler(r, COAP_REQUEST_DELETE, hnd_delete_resource);

    if (ins.s) {
        buf = (unsigned char *) coap_malloc(ins.length + 2);
        if (buf) {
            buf[0] = '"';
            memcpy(buf + 1, ins.s, ins.length);
            buf[ins.length + 1] = '"';
            attr_val.s = buf;
            attr_val.length = ins.length + 2;
            coap_add_attr(r,
                          coap_make_str_const("ins"),
                          &attr_val,
                          0);
            coap_free(buf);
        }
    }

    if (rt.s) {
        buf = (unsigned char *) coap_malloc(rt.length + 2);
        if (buf) {
            /* add missing quotes */
            buf[0] = '"';
            memcpy(buf + 1, rt.s, rt.length);
            buf[rt.length + 1] = '"';
            attr_val.s = buf;
            attr_val.length = rt.length + 2;
            coap_add_attr(r,
                          coap_make_str_const("rt"),
                          &attr_val,
                          0);
            coap_free(buf);
        }
    }

    // TODO: take ip:port from the query string
    add_source_address(r, coap_session_get_addr_remote(session));

    // add created resource to the RD
    coap_add_resource(coap_session_get_context(session), r);

    // create the response
    coap_pdu_set_code(response, COAP_RESPONSE_CODE_CREATED);

    {
        unsigned char _b[loc_const];
        unsigned char *b = _b;
        size_t buf_len = sizeof(_b);
        int n_seg;

        n_seg = coap_split_path(loc, loc_size, b, &buf_len);

        while (n_seg--) {
            coap_add_option(response,
                            COAP_OPTION_LOCATION_PATH,
                            coap_opt_length(b),
                            coap_opt_value(b));
            b += coap_opt_size(b);
        }
    }
}

/**
 * Create the context for the Resource Directory
 * @param node the IP address of the Resource Directory
 * @param port the PORT of the resource Directory (DEFAULT: 5683)
 * @return the context for the Resource Directory
 */
static coap_context_t *get_context(const char *node, const char *port) {
    coap_context_t *ctx;
    int s;
    struct addrinfo hints{};
    struct addrinfo *result, *rp;

    ctx = coap_new_context(nullptr);
    if (!ctx) {
        return nullptr;
    }

    /* Need PSK set up before we set up (D)TLS endpoints */
    //fill_keystore(ctx);

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_DGRAM; /* Coap uses UDP */
    hints.ai_flags = AI_PASSIVE | AI_NUMERICHOST;

    s = getaddrinfo(node, port, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        coap_free_context(ctx);
        return nullptr;
    }

    /* iterate through results until success */
    for (rp = result; rp != nullptr; rp = rp->ai_next) {
        coap_address_t addr, addrs;
        coap_endpoint_t *ep_udp, *ep_tcp;

        if (rp->ai_addrlen <= (socklen_t) sizeof(addr.addr)) {
            coap_address_init(&addr);
            addr.size = (socklen_t) rp->ai_addrlen;
            memcpy(&addr.addr, rp->ai_addr, rp->ai_addrlen);
            addrs = addr;
            if (addr.addr.sa.sa_family == AF_INET) {
                uint16_t temp = ntohs(addr.addr.sin.sin_port) + 1;
                addrs.addr.sin.sin_port = htons(temp);
            } else if (addr.addr.sa.sa_family == AF_INET6) {
                uint16_t temp = ntohs(addr.addr.sin6.sin6_port) + 1;
                addrs.addr.sin6.sin6_port = htons(temp);
            } else {
                freeaddrinfo(result);
                return ctx;
            }

            ep_udp = coap_new_endpoint(ctx, &addr, COAP_PROTO_UDP);
            if (!ep_udp) {
                coap_log(LOG_CRIT, "cannot create UDP endpoint\n");
            }
            ep_tcp = coap_new_endpoint(ctx, &addr, COAP_PROTO_TCP);
            if (!ep_tcp) {
                coap_log(LOG_CRIT, "cannot create TCP endpoint\n");
            }
            if (ep_udp) {
                freeaddrinfo(result);
                return ctx;
            }
        }
    }

    fprintf(stderr, "no context available for interface '%s'\n", node);

    freeaddrinfo(result);
    return ctx;
}

static void init_resources(coap_context_t *context) {
    coap_resource_t *r;

    r = coap_resource_init(coap_make_str_const(RD_ROOT_STR), 0);
    coap_register_handler(r, COAP_REQUEST_GET, hnd_get_rd);
    coap_register_handler(r, COAP_REQUEST_POST, hnd_post_rd);

    coap_add_attr(r, coap_make_str_const("ct"), coap_make_str_const("40"), 0);
    coap_add_attr(r, coap_make_str_const("rt"), coap_make_str_const("\"core.rd\""), 0);
    coap_add_attr(r, coap_make_str_const("ins"), coap_make_str_const("\"default\""), 0);

    coap_add_resource(context, r);

    coap_resource_release_userdata_handler(context, resource_rd_delete);
}

int run() {
    int quit = 0;
    char address_string[NI_MAXHOST] = "0.0.0.0";
    // "192.168.159.128"
    char port_string[NI_MAXHOST] = "5683";

    coap_context_t *context;
    coap_log_t log_level = LOG_DEBUG;

    // START THE SERVER
    coap_startup();
    coap_set_log_level(log_level);

    context = get_context(address_string, port_string);
    if (!context) {
        return -1;
    }

//    coap_address_t address;
    coap_join_mcast_group(context, "224.0.1.187");
//    resolve_address("224.0.1.187", "5683", &address);

    //coap_new_endpoint(context, &address, COAP_PROTO_UDP);

    // INITIALIZE RESOURCE DIRECTORY RESOURCES
    init_resources(context);

    // PROCESS REQUESTS
    while (!quit) {
        int result = coap_io_process(context, COAP_RESOURCE_CHECK_TIME * 1000);
    }

    return 0;
}

int main() {
    run();
}
