/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */

#include <coap3/coap.h>
#include <unordered_map>
#include <netdb.h>
#include <vector>
#include "CoapClient.h"

#define RD_STR "</rd>"
#define IP_STRING "addr="
#define PORT_STRING "port="
#define SERVER_LEISURE_TIME (5U * 2)
#define RD_ROOT_STR "rd"
#define RD_ROOT_SIZE 2

class CoapClient;

class CoapServer {
private:
    int quit = 0;
    coap_context_t *context = nullptr;
    coap_endpoint_t *endpoint = nullptr;
    static ThreadSafeMap<std::string, coap_resource_t *> *resources;
    std::string resource_dir_ip;
    std::string resource_dir_port;

    coap_resource_t *resource_dir_local = nullptr;
    static ThreadSafeMap<std::string, coap_resource_t *> *rd_resources;

    CoapClient *client;

    static int resolve_address(const char *host,
                               const char *service,
                               coap_address_t *dst);

    void destroyServer();

    static std::vector<std::string> split(const std::string &s, char delimiter);


public:
    CoapServer();

    explicit CoapServer(CoapClient *client);

    explicit CoapServer(const char *host, const char *port, int is_resource_dir);

    int get_quit() const {
        return this->quit;
    };

    void set_quit(int val) {
        this->quit = val;
    };

    coap_context_t *get_context();

    CoapClient *get_client();

    int init_server(const char *host, const char *port);

    void add_resource(const char *uri, int flags, const char *attr_list);

    void publish_resource(const char *uri);

    int delete_resource(const char *uri);

    void unpublish_resource(const char *uri);

    // ### RESOURCE DIR FUNCTIONS ###

    /**
     * Initialize the resource directory on this node
     */
    void init_resource_dir_local();

    /**
     * Handler for GET requests on rd
     * @param response The response crafted by the Registry Directory
     */
    static void hnd_get_rd(coap_resource_t *resource,
                           coap_session_t *session,
                           const coap_pdu_t *request,
                           const coap_string_t *query,
                           coap_pdu_t *response);

    /**
     * Handler for the POST request on rd
     * @param session   Session of the Registry Directory
     * @param request   Request to be served
     * @param response  Response to the currently server request
     */
    static void hnd_post_rd(coap_resource_t *resource,
                            coap_session_t *session,
                            const coap_pdu_t *request,
                            const coap_string_t *query,
                            coap_pdu_t *response);

    /**
     * Handler for GET requests of resources registered in the Registry Directory
     * @param resource  the resource targeted by the GET request
     * @param response  the answer to the GET request; assumed that the resource is present in the RD
     * */
    static void hnd_get_resource(coap_resource_t *resource,
                                 coap_session_t *session,
                                 const coap_pdu_t *request,
                                 const coap_string_t *query,
                                 coap_pdu_t *response);

    /**
     * Handler for PUT requests: update the state of the specified resource
     * @param query     the query string (format: ?key1=value1&key2=value2) that will be parsed
     * @param response  the answer to the PUT request TODO: spiegare i codici
     * */
    static void hnd_put_resource(coap_resource_t *resource,
                                 coap_session_t *session,
                                 const coap_pdu_t *request,
                                 const coap_string_t *query,
                                 coap_pdu_t *response);

    /**
     * Handler for DELETE requests of resources registered in the Registry Directory
     * @param resource  the resource targeted by the DELETE request
     * @param response  2.05 if the resource is deleted, 4.04 if the resource was not found
     * */
    static void hnd_delete_resource(coap_resource_t *resource,
                                    coap_session_t *session,
                                    const coap_pdu_t *request,
                                    const coap_string_t *query,
                                    coap_pdu_t *response);

    static void resource_rd_delete(void *ptr);

    static void resource_handler_get(coap_resource_t *resource,
                                     coap_session_t *session,
                                     const coap_pdu_t *request,
                                     const coap_string_t *query,
                                     coap_pdu_t *response);

    static void resource_handler_put(coap_resource_t *resource,
                                     coap_session_t *session,
                                     const coap_pdu_t *request,
                                     const coap_string_t *query,
                                     coap_pdu_t *response);

    static void resource_handler_post(coap_resource_t *resource,
                                      coap_session_t *session,
                                      const coap_pdu_t *request,
                                      const coap_string_t *query,
                                      coap_pdu_t *response);

    void check_resource_expiration();

    int run();

};
