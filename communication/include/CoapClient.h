/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */

#ifndef EDDIE_COAPCLIENT_H
#define EDDIE_COAPCLIENT_H

#include <coap3/coap.h>
#include <mutex>
#include <unordered_map>
#include <unordered_set>
#include <list>
#include <condition_variable>
#include <thread>

#define COAP_IP4_MULTICAST "224.0.1.187"
#define COAP_IP6_MULTICAST_LOCAL_LINK "FF02::FD"
#define COAP_IP6_MULTICAST_SITE_LOCAL "FF05::FD"

/**
 * Structure that holds responses received by the client.
 * @tparam Key      message_id of the response
 * @tparam Value    pointer to the response in memory
 */
template<class Key, class Value>
class ThreadSafeMap {
    std::unordered_map<Key, Value> c_;

public:
    std::mutex m_;

    Value get(Key const &k) {
        std::unique_lock<decltype(m_)> lock(m_);
        Value elem = c_[k];
        // m_.unlock()
        return elem;
    }

    template<class Value2>
    void set(Key const &k, Value2 &&v) {
        std::unique_lock<decltype(m_)> lock(m_);
        c_[k] = std::forward<Value2>(v);
        // m_.unlock()
    }

    void erase(Key const &k) {
        std::unique_lock<decltype(m_)> lock(m_);
        Value elem = this->get(k);
        c_.erase(k);
        // m_.unlock()
    }
};

template<class Key, class Value>
class ThreadSafeStorage {
    std::unordered_multimap<Key, Value> mm_;
    std::unordered_set<Key> s_;

public:
    std::mutex m_;

    /**
     * Add a new key to the set of valid keys
     * @param k the key to add
     */
    void insert_valid_key(Key const &k) {
        std::unique_lock<decltype(m_)> lock(m_);
        s_.insert(k);
    }

    /**
     * Remove a key from the set of valid keys.
     * As a side effect, all pairs in the unordered_multimap are removed
     *
     * @param k the key to remove
     */
    void remove_valid_key (Key const &k) {
        std::unique_lock<decltype(m_)> lock(m_);

        // erase key and remove all messages
        s_.erase(k);
        mm_.erase(k);
    }

    /**
     * Check if the provided key is contained in the set of valid keys.
     * @param k     the key to find in the set of valid keys
     * @return      true if the key is found, false otherwise
     */
    int contains_key(Key const &k) {
        // lock the storage
        std::unique_lock<decltype(m_)> lock(m_);

        // check if the set contains the key
        auto result = s_.find(k);
        return result != s_.end();
    }

    template<class Value2>
    void insert_new_message(Key const &k, Value2 &&v) {
        std::unique_lock<decltype(m_)> lock(m_);

        if (s_.find(k) != s_.end()) { // the key is present, add the message
            mm_.insert(std::make_pair(k, v));
        }
        // key not present, ignore the message
    }

    std::list<Value> get(Key const &k) {
        std::unique_lock<decltype(m_)> lock(m_);
        std::list<Value> return_list;

        if (s_.find(k) != s_.end()) { // work only if the key is valid

            auto items = mm_.equal_range(k);

            for (auto item = items.first; item != items.second; ++item) {
                return_list.push_back(item->second);
            }
        }

        return return_list;
    }
};


class CoapClient {
private:
    int quit = 0;
    coap_context_t *context = nullptr;
    std::thread runner;

    static int resolve_address(const char *host, const char *service, coap_address_t *dst);

    coap_session_t *open_session(const char *dst_host, const char *dst_port, coap_proto_t protocol);

    void destroyClient();

public:
    static ThreadSafeMap<int, coap_pdu_t *> *messages;
    static ThreadSafeStorage<std::string, coap_pdu_t *> *multicast_messages;
    std::mutex client_mutex;
    std::condition_variable client_condition;

    CoapClient();

    int get_quit() const;

    void set_quit(int value);

    coap_context_t *get_context();

    int initClient();

    static coap_response_t
    message_handler(coap_session_t *session,
                    const coap_pdu_t *sent,
                    const coap_pdu_t *received,
                    coap_mid_t id);

    coap_mid_t send_get_request(const char *dst_host,
                                const char *dst_port,
                                const char *dst_resource,
                                const char *query,
                                unsigned char *data,
                                size_t data_length);

    std::string send_get_request_non_confirmable(const char *destination,
                                                           const char *query,
                                                           unsigned char *data,
                                                           size_t data_length);

    coap_mid_t send_post_request(const char *dst_host,
                                 const char *dst_port,
                                 const char *dst_resource,
                                 const char *query);

    coap_mid_t send_delete_request(const char *dst_host,
                                   const char *dst_port,
                                   const char *res_to_delete);

    coap_mid_t send_put_request(const char *dst_host,
                                const char *dst_port,
                                const char *dst_resource,
                                const char *query,
                                const char *data);

    int run();
};

#endif //EDDIE_COAPCLIENT_H
