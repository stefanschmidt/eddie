/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */

#include <coap3/coap.h>
#include <netdb.h>
#include <cstdio>
#include <functional>
#include "CoapClient.h"

/**
 * Container for all response received from the various CoAP nodes
 */
ThreadSafeMap<int, coap_pdu_t *> *CoapClient::messages = new ThreadSafeMap<int, coap_pdu_t *>();

/**
 * Container for all multicast messages received
 */
ThreadSafeStorage<std::string, coap_pdu_t *> *CoapClient::multicast_messages = new ThreadSafeStorage<std::string, coap_pdu_t *>();

int CoapClient::resolve_address(const char *host, const char *service, coap_address_t *dst) {
    struct addrinfo *res, *aInfo;
    struct addrinfo hints{};
    int error, len = -1;

    memset(&hints, 0, sizeof(hints));
    memset(dst, 0, sizeof(*dst));
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_family = AF_UNSPEC;

    error = getaddrinfo(host, service, &hints, &res);

    if (error != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(error));
        return error;
    }

    for (aInfo = res; aInfo != nullptr; aInfo = aInfo->ai_next) {
        switch (aInfo->ai_family) {
            case AF_INET6:
            case AF_INET:
                len = dst->size = aInfo->ai_addrlen;
                memcpy(&dst->addr.sin6, aInfo->ai_addr, dst->size);
                freeaddrinfo(res);
                return len;
            default:;
        }
    }

    freeaddrinfo(res);
    return len;
}

/**
 * Handler used for coap_send() responses
 * @param session   current open session
 * @param sent      the pdu request sent to the server
 * @param received  the pdu response received from the ser
 * @param id        the id of the pdu received
 * @return          the response code to sent back as an ACK
 */
coap_response_t
CoapClient::message_handler(coap_session_t *session,
                            const coap_pdu_t *sent,
                            const coap_pdu_t *received,
                            const coap_mid_t id) {

    // TODO: query check of incoming pdu

    if (sent) { // response to end-to-end request
        coap_pdu_t *copy = coap_pdu_duplicate(received, session,
                                              coap_pdu_get_token(received).length,
                                              coap_pdu_get_token(received).s,
                                              nullptr);

        coap_pdu_set_mid(copy, id);

        auto *len = static_cast<size_t *>(malloc(sizeof(size_t)));
        const auto **data = static_cast<const uint8_t **>(malloc(sizeof(const uint8_t *)));

        coap_get_data(received, len, data);
        coap_add_data(copy, *len, *data);

        // add response to the container
        // so that higher layers can retrieve it
        messages->set(id, copy);

        return COAP_RESPONSE_OK;
    } else { // response to multicast request
        auto token = coap_pdu_get_token(received);
        std::string token_str = std::string(reinterpret_cast<const char *>(token.s)).substr(0, token.length);

        if (multicast_messages->contains_key(token_str)) {
            auto *len = static_cast<size_t *>(malloc(sizeof(size_t)));
            const auto **data = static_cast<const uint8_t **>(malloc(sizeof(const uint8_t *)));

            coap_pdu_t *copy = coap_pdu_duplicate(received, session,
                                                  coap_pdu_get_token(received).length,
                                                  coap_pdu_get_token(received).s,
                                                  nullptr);

            coap_get_data(received, len, data);
            coap_add_data(copy, *len, *data);

            // add copy to the container
            multicast_messages->insert_new_message(token_str, copy);

            return COAP_RESPONSE_OK;
        } else {// token no more tracked
            // discard message and do nothing

            return COAP_RESPONSE_OK;
        }
    }
}

/**
 * Create a client session to the specified destination
 * @param dst_host the IP address of the node to contact
 * @param dst_port the port where to contact the node
 * @param protocol the protocol to use for the communication
 * @return the newly opened client session
 */
coap_session_t *CoapClient::open_session(const char *dst_host, const char *dst_port, coap_proto_t protocol) {
    coap_session_t *session;
    coap_address_t dst;

    if (resolve_address(dst_host, dst_port, &dst) < 0) {
        coap_log(LOG_CRIT, "Unable to open connection\n");
        return nullptr;
    }

    session = coap_new_client_session(this->context, nullptr, &dst, protocol);

    if (!session) {
        coap_log(LOG_CRIT, "Unable to initiate session\n");
        coap_session_release(session);
        return nullptr;
    }

    return session;
}

void CoapClient::destroyClient() {
    coap_free_context(this->context);
    coap_cleanup();
}

CoapClient::CoapClient() {
    this->context = nullptr;
}

int CoapClient::get_quit() const {
    return this->quit;
}

void CoapClient::set_quit(int value) {
    this->quit = value;
}

coap_context_t *CoapClient::get_context() {
    return this->context;
}

int CoapClient::initClient() {
    this->context = coap_new_context(nullptr);

    if (!this->context) {
        coap_log(LOG_EMERG, "Unable to create context\n");
        coap_free_context(this->context);
        coap_cleanup();

        return -1;
    }

    coap_register_response_handler(this->context, message_handler);

    return 0;
}

/**
 * Send COAP GET request to specified destination, on port 5683, with specified query and data
 * @param destination   the ip address of the node to contact
 * @param query         the string containing the query associated to the GET request
 */
coap_mid_t
CoapClient::send_get_request(const char *dst_host,
                             const char *dst_port,
                             const char *dst_resource,
                             const char *query,
                             unsigned char *data,
                             size_t data_length) {
    coap_session_t *session;
    uint8_t buf[1024];
    size_t buflen;
    uint8_t *sbuf = buf;
    coap_pdu_t *pdu;
    int res;
    coap_optlist_t *optlist = nullptr;
    coap_mid_t message_id;

    if (!dst_port) {
        dst_port = "5683";
    }

    // BEGIN CRITICAL CALLS TO CoAP APIs
    std::unique_lock<decltype(this->client_mutex)> lock(this->client_mutex);

    session = open_session(dst_host, dst_port, COAP_PROTO_UDP);

    if (!session) {
        coap_log(LOG_EMERG, "Error creating remote session\n");
        coap_session_release(session);
        return -1;
    }

    // Initialize the PDU to send
    message_id = coap_new_message_id(session);
    pdu = coap_pdu_init(COAP_MESSAGE_CON, COAP_REQUEST_CODE_GET,
                        message_id,
                        coap_session_max_pdu_size(session));

    // Add token to the request
    coap_session_new_token(session, &buflen, buf);
    coap_add_token(pdu, buflen, buf);

    // Add destination as option to the PDU
    if (dst_resource) {
        buflen = sizeof(buf);
        res = coap_split_path((const uint8_t *) dst_resource,
                              strlen(dst_resource),
                              sbuf,
                              &buflen);

        while (res--) {
            coap_insert_optlist(&optlist,
                                coap_new_optlist(COAP_OPTION_URI_PATH,
                                                 coap_opt_length(sbuf),
                                                 coap_opt_value(sbuf)));
            sbuf += coap_opt_size(sbuf);
        }
    } else {
        coap_delete_pdu(pdu);
        coap_delete_optlist(optlist);
        return -1;
    }

    // Add query as option to the PDU
    if (query) {
        buflen = sizeof(buf);
        res = coap_split_query((const uint8_t *) query, strlen(query), sbuf, &buflen);

        while (res--) {
            coap_insert_optlist(&optlist,
                                coap_new_optlist(COAP_OPTION_URI_QUERY,
                                                 coap_opt_length(sbuf),
                                                 coap_opt_value(sbuf)));

            sbuf += coap_opt_size(sbuf);
        }
    }

    coap_add_optlist_pdu(pdu, &optlist);

    // check data and data type
    if (data && data_length) {
        //TODO: check if useful
        coap_add_data(pdu, data_length, data);
    }

    coap_send(session, pdu);

    // END OF CRITICAL LINES
    // unlock the mutex
    lock.unlock();

    return message_id;
}

std::string
CoapClient::send_get_request_non_confirmable(const char *dst_resource,
                                              const char *query,
                                              unsigned char *data,
                                              size_t data_length) {
    coap_session_t *session;
    uint8_t buf[1024];
    size_t buflen;
    uint8_t *sbuf = buf;
    coap_pdu_t *pdu;
    int res;
    coap_optlist_t *optlist = nullptr;

    uint8_t token_buf[1024];
    memset(token_buf, 0, sizeof(token_buf));
    size_t token_len;
    uint8_t *token = token_buf;

    session = open_session("224.0.1.187", "5683", COAP_PROTO_UDP);

    if (!session) {
        coap_log(LOG_EMERG, "Error creating remote session\n");
        coap_session_release(session);
        return "";
    }

    // Initialize the PDU to send
    pdu = coap_pdu_init(COAP_MESSAGE_NON, COAP_REQUEST_CODE_GET,
                        coap_new_message_id(session),
                        coap_session_max_pdu_size(session));

    // Add token to the request
    coap_session_new_token(session, &token_len, token);
    coap_add_token(pdu, token_len, token);

    // Add destination as option to the PDU
    if (dst_resource) {
        buflen = sizeof(buf);
        res = coap_split_path((const uint8_t *) dst_resource,
                              strlen(dst_resource),
                              sbuf,
                              &buflen);

        while (res--) {
            coap_insert_optlist(&optlist,
                                coap_new_optlist(COAP_OPTION_URI_PATH,
                                                 coap_opt_length(sbuf),
                                                 coap_opt_value(sbuf)));
            sbuf += coap_opt_size(sbuf);
        }
    } else {
        coap_delete_pdu(pdu);
        coap_delete_optlist(optlist);
        return "";
    }

    // Add query as option to the PDU
    if (query) {
        buflen = sizeof(buf);
        res = coap_split_query((const uint8_t *) query, strlen(query), sbuf, &buflen);

        while (res--) {
            coap_insert_optlist(&optlist,
                                coap_new_optlist(COAP_OPTION_URI_QUERY,
                                                 coap_opt_length(sbuf),
                                                 coap_opt_value(sbuf)));

            sbuf += coap_opt_size(sbuf);
        }
    }

    coap_add_optlist_pdu(pdu, &optlist);

    // check data and data type
    if (data && data_length) {
        //TODO: check if useful
        coap_add_data(pdu, data_length, data);
    }

    // add token string as valid token
    std::string token_str = std::string(reinterpret_cast<const char *>(token));
    multicast_messages->insert_valid_key(token_str);

    coap_send(session, pdu);

    return token_str;
}

coap_mid_t
CoapClient::send_post_request(const char *dst_host,
                              const char *dst_port,
                              const char *dst_resource,
                              const char *query) {
    coap_mid_t message_id;
    coap_session_t *session;
    uint8_t buf[1024];
    size_t buflen;
    uint8_t *sbuf = buf;
    coap_pdu_t *pdu;
    int res;
    coap_optlist_t *optlist = nullptr;

    // BEGIN CRITICAL CALLS TO CoAP APIs
    std::unique_lock<decltype(this->client_mutex)> lock(this->client_mutex);

    session = open_session(dst_host, dst_port, COAP_PROTO_UDP);

    if (!session) {
        coap_log(LOG_EMERG, "Error creating remote session\n");
        coap_session_release(session);
        return -1;
    }

    // Initialize the PDU to send
    message_id = coap_new_message_id(session);
    pdu = coap_pdu_init(COAP_MESSAGE_CON, COAP_REQUEST_CODE_POST,
                        message_id,
                        coap_session_max_pdu_size(session));

    // Add token to the request
    coap_session_new_token(session, &buflen, buf);
    coap_add_token(pdu, buflen, buf);

    // Add destination as option to the PDU
    if (dst_resource) {
        buflen = sizeof(buf);
        res = coap_split_path((const uint8_t *) dst_resource,
                              strlen(dst_resource),
                              sbuf,
                              &buflen);

        while (res--) {
            coap_insert_optlist(&optlist,
                                coap_new_optlist(COAP_OPTION_URI_PATH,
                                                 coap_opt_length(sbuf),
                                                 coap_opt_value(sbuf)));
            sbuf += coap_opt_size(sbuf);
        }
    } else {
        coap_delete_pdu(pdu);
        coap_delete_optlist(optlist);
        return -1;
    }

    // Add query as option to the PDU
    if (query) {
        buflen = sizeof(buf);
        res = coap_split_query((const uint8_t *) query, strlen(query), sbuf, &buflen);

        while (res--) {
            coap_insert_optlist(&optlist,
                                coap_new_optlist(COAP_OPTION_URI_QUERY,
                                                 coap_opt_length(sbuf),
                                                 coap_opt_value(sbuf)));

            sbuf += coap_opt_size(sbuf);
        }
    } else {
        coap_delete_pdu(pdu);
        coap_delete_optlist(optlist);
        return -1;
    }

    coap_add_optlist_pdu(pdu, &optlist);

    coap_send(session, pdu);

    // END OF CRITICAL LINES
    // unlock the mutex
    lock.unlock();

    return message_id;
}

coap_mid_t
CoapClient::send_delete_request(const char *dst_host,
                                const char *dst_port,
                                const char *res_to_delete) {
    if (!res_to_delete) {
        coap_log(LOG_DEBUG, "[send_delete_request]: `res_to_delete` can not be empty\n");
        return -1;
    }

    coap_mid_t message_id;
    coap_session_t *session;
    uint8_t buf[1024];
    size_t buflen;
    uint8_t *sbuf = buf;
    coap_pdu_t *pdu;
    int res;
    coap_optlist_t *optlist = nullptr;

    // BEGIN CRITICAL CALLS TO CoAP APIs
    std::unique_lock<decltype(this->client_mutex)> lock(this->client_mutex);

    session = open_session(dst_host, dst_port, COAP_PROTO_UDP);

    if (!session) {
        coap_log(LOG_EMERG, "Error creating remote session\n");
        coap_session_release(session);
        return -1;
    }

    message_id = coap_new_message_id(session);

    // Initialize the PDU to send
    pdu = coap_pdu_init(COAP_MESSAGE_CON, COAP_REQUEST_CODE_DELETE,
                        message_id,
                        coap_session_max_pdu_size(session));

    // Add token to the request
    coap_session_new_token(session, &buflen, buf);
    coap_add_token(pdu, buflen, buf);

    if (res_to_delete) {
        buflen = sizeof(buf);
        res = coap_split_path((const uint8_t *) res_to_delete,
                              strlen(res_to_delete),
                              sbuf,
                              &buflen);

        while (res--) {
            coap_insert_optlist(&optlist,
                                coap_new_optlist(COAP_OPTION_URI_PATH,
                                                 coap_opt_length(sbuf),
                                                 coap_opt_value(sbuf)));
            sbuf += coap_opt_size(sbuf);
        }
    } else {
        coap_delete_pdu(pdu);
        coap_delete_optlist(optlist);
        return -1;
    }

    coap_add_optlist_pdu(pdu, &optlist);

    coap_send(session, pdu);

    // END OF CRITICAL LINES
    // unlock the mutex
    lock.unlock();

    return message_id;
}

coap_mid_t
CoapClient::send_put_request(const char *dst_host,
                             const char *dst_port,
                             const char *dst_resource,
                             const char *query,
                             const char *data) {
    coap_mid_t message_id;
    coap_session_t *session;
    uint8_t buf[1024];
    size_t buflen;
    uint8_t *sbuf = buf;
    coap_pdu_t *pdu;
    int res;
    coap_optlist_t *optlist = nullptr;

    // BEGIN CRITICAL CALLS TO CoAP APIs
    std::unique_lock<decltype(this->client_mutex)> lock(this->client_mutex);

    session = open_session(dst_host, dst_port, COAP_PROTO_UDP);

    if (!session) {
        coap_log(LOG_EMERG, "Error creating remote session\n");
        coap_session_release(session);
        return -1;
    }

    message_id = coap_new_message_id(session);

    pdu = coap_pdu_init(COAP_MESSAGE_CON,
                        COAP_REQUEST_CODE_PUT,
                        message_id,
                        coap_session_max_pdu_size(session));

    // Add token to the request
    coap_session_new_token(session, &buflen, buf);
    coap_add_token(pdu, buflen, buf);

    if (dst_resource) {
        buflen = sizeof(buf);
        res = coap_split_path((const uint8_t *) dst_resource,
                              strlen(dst_resource),
                              sbuf,
                              &buflen);

        while (res--) {
            coap_insert_optlist(&optlist,
                                coap_new_optlist(COAP_OPTION_URI_PATH,
                                                 coap_opt_length(sbuf),
                                                 coap_opt_value(sbuf)));
            sbuf += coap_opt_size(sbuf);
        }
    } else {
        // error, destination needed
        coap_delete_pdu(pdu);
        coap_delete_optlist(optlist);
        return -1;
    }

    // Add query as option to the PDU
    if (query) {
        buflen = sizeof(buf);
        res = coap_split_query((const uint8_t *) query, strlen(query), sbuf, &buflen);

        while (res--) {
            coap_insert_optlist(&optlist,
                                coap_new_optlist(COAP_OPTION_URI_QUERY,
                                                 coap_opt_length(sbuf),
                                                 coap_opt_value(sbuf)));

            sbuf += coap_opt_size(sbuf);
        }
    }

    coap_add_optlist_pdu(pdu, &optlist);

    coap_add_data(pdu, strlen(data), reinterpret_cast<const uint8_t *>(data));

    coap_send(session, pdu);

    // END OF CRITICAL LINES
    // unlock the mutex
    lock.unlock();

    return message_id;
}

int CoapClient::run() {
    while (!this->get_quit()) {
        coap_io_process(this->get_context(), COAP_IO_WAIT * 1000);
    }

    this->destroyClient();

    return 0;
}
