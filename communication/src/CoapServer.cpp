/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */

#include <coap3/coap.h>
#include <netdb.h>
#include <sys/socket.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unordered_map>
#include <condition_variable>
#include <thread>
#include <sstream>

#include "CoapServer.h"

#define min(a, b) ((a) < (b) ? (a) : (b))

class CoapClient;

ThreadSafeMap<std::string, coap_resource_t *> *CoapServer::rd_resources = new ThreadSafeMap<std::string, coap_resource_t *>();
ThreadSafeMap<std::string, coap_resource_t *> *CoapServer::resources = new ThreadSafeMap<std::string, coap_resource_t *>();

// TODO: secondary thread that check resource expiration
// TODO: check if the resource work is done with the map in every function

// ### PRIVATE ###
int CoapServer::resolve_address(const char *host,
                                const char *service,
                                coap_address_t *dst) {
    struct addrinfo *res, *aInfo;
    struct addrinfo hints{};
    int error, len = -1;

    memset(&hints, 0, sizeof(hints));
    memset(dst, 0, sizeof(*dst));
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_family = AF_UNSPEC;

    error = getaddrinfo(host, service, &hints, &res);

    if (error != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(error));
        return error;
    }

    for (aInfo = res; aInfo != nullptr; aInfo = aInfo->ai_next) {
        switch (aInfo->ai_family) {
            case AF_INET6:
            case AF_INET:
                len = dst->size = aInfo->ai_addrlen;
                memcpy(&dst->addr.sin6, aInfo->ai_addr, dst->size);
                freeaddrinfo(res);
                return len;
            default:;
        }
    }

    freeaddrinfo(res);
    return len;
}

void CoapServer::destroyServer() {
    // TODO: free all maps with resources <- method should be added in the ThreadSafe... classes
    coap_free_context(this->context);

    coap_cleanup();
}

/**
 * Split a string in different substrings, given the delimiter character.
 *
 * @param s             the string to analyze
 * @param delimiter     the delimiter character of every substring
 */
std::vector<std::string> CoapServer::split(const std::string &s, char delimiter) {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);

    while (std::getline(tokenStream, token, delimiter)) {
        tokens.push_back(token);
    }

    return tokens;
}


// ### PUBLIC ###
CoapServer::CoapServer() {
    this->context = nullptr;
    this->endpoint = nullptr;
    this->client = new CoapClient();
    this->client->initClient();
    this->resource_dir_local = nullptr;
}

CoapServer::CoapServer(CoapClient *client) {
    this->context = nullptr;
    this->endpoint = nullptr;
    this->client = client;
}

CoapServer::CoapServer(const char *host, const char *port, int is_resource_dir) {
    init_server(host, port);
    this->client = new CoapClient();
    this->client->initClient();
    if (is_resource_dir) {
        init_resource_dir_local();
    } else {
        this->resource_dir_local = nullptr;
    }
}

coap_context_t *CoapServer::get_context() {
    return this->context;
}

CoapClient *CoapServer::get_client() {
    return this->client;
}

int CoapServer::init_server(const char *host, const char *port) {
    coap_address_t dst;

    if (!port) {
        port = "5683";
    }

    coap_startup();

    if (resolve_address(host, port, &dst) < 0) {
        coap_log(LOG_CRIT, "Failed to resolve address\n");
        coap_free_context(this->context);
        coap_cleanup();
        return EXIT_FAILURE;
    }

    this->context = coap_new_context(nullptr);
    this->endpoint = coap_new_endpoint(context, &dst, COAP_PROTO_UDP);

    if (!this->context || !this->endpoint) {
        coap_log(LOG_EMERG, "Cannot initialize context\n");
        coap_free_context(this->context);
        coap_cleanup();
        return EXIT_FAILURE;
    }

    // be available on multicast groups
    coap_join_mcast_group(this->context, COAP_IP4_MULTICAST);
    coap_join_mcast_group(this->context, COAP_IP6_MULTICAST_LOCAL_LINK);
    coap_join_mcast_group(this->context, COAP_IP6_MULTICAST_SITE_LOCAL);

    return EXIT_SUCCESS;
}

/**
 * Add a new resource to a CoapServer.
 * @param uri           The name of the resource
 * @param flags         libcoap param (usually 0)
 * @param attr_list     a string containing the list of attributes of the resource
 */
void CoapServer::add_resource(const char *uri, int flags, const char *attr_list) {
    coap_resource_t *resource = coap_resource_init(coap_make_str_const(uri), flags);

    coap_register_handler(resource, COAP_REQUEST_GET, resource_handler_get);
    coap_register_handler(resource, COAP_REQUEST_PUT, resource_handler_put);

    // add specified attributes to resource
    auto list = split(std::string(attr_list), '&');
    auto attr_map = std::unordered_map<std::string, std::string>();

    for (const auto &pair: list) {
        auto key = split(pair, '=')[0];
        auto value = split(pair, '=')[1];

        coap_add_attr(resource,
                      coap_make_str_const(key.c_str()),
                      coap_make_str_const(value.c_str()),
                      0);
    }

    coap_add_resource(this->context, resource);
    CoapServer::resources->set(uri, resource);
}

void CoapServer::publish_resource(const char *uri) {
    // build the query for the resource directory
    std::string query_string = std::string("h=") + std::string(uri);
    if (resource_dir_ip.empty()) {
        // get the ip of the resource directory
        // "rt=core.rd"
        auto token = this->client->send_get_request_non_confirmable(".well-known/core",
                                                                    nullptr,
                                                                    nullptr,
                                                                    0);

        // acquire the lock
        std::unique_lock<decltype(this->client->client_mutex)> lock(this->client->client_mutex);

        // wait for leisure time to expire
        auto timeout = std::chrono::system_clock::now() + std::chrono::seconds(SERVER_LEISURE_TIME);
        this->client->client_condition.wait_until(lock, timeout);

        // thread is now awake, control if the messages are in the storage of the client
        auto list = CoapClient::multicast_messages->get(token);

        if (list.empty()) {
            // list is empty => no resource directory in the network
            // start creation of resource directory

            // FIXME: complete implementation
        } else {
            // list has content => res_dir exists
            // assume that there is only one `rt=core.rd` node in the network
            auto pdu = list.front();

            // analyze content
            // take data in pdu
            auto *data_len = static_cast<size_t *>(malloc(sizeof(size_t)));
            const auto **data = static_cast<const uint8_t **>(malloc(sizeof(const uint8_t *)));

            coap_get_data(pdu, data_len, data);

            auto response_str = std::string(reinterpret_cast<const char *>(*data));

            // split the resources, identified by ','
            auto resources_vector = split(response_str, ',');

            // find the resource with `</rd>`
            std::string rd_str;
            for (const auto &res: resources_vector) {
                if (res.find(RD_STR) != std::string::npos) {
                    rd_str = res;
                }
            }

            // get ip and port from the response
            auto start_ip = rd_str.find(IP_STRING) + strlen(IP_STRING) + 1;
            auto end_ip = rd_str.find(';', start_ip);

            std::string ip = rd_str.substr(start_ip, end_ip - start_ip - 1);

            auto start_port = rd_str.find(PORT_STRING) + strlen(PORT_STRING) + 1;
            auto end_port = rd_str.find(';', start_port);

            std::string port = rd_str.substr(start_port, end_port - start_port - 1);

            // store the values for future reference
            this->resource_dir_ip = ip;
            this->resource_dir_port = port;

            // de-alloc all items in list
            for (auto item: list) {
                coap_delete_pdu(item);
            }

            // send POST request to the resource_dir
            this->client->send_post_request(resource_dir_ip.c_str(),
                                            resource_dir_port.c_str(),
                                            "rd",
                                            query_string.c_str());
        }

        // remove token from valid tokens
        CoapClient::multicast_messages->remove_valid_key(token);
    } else {
        // resource directory is known
        // send the request directly

        this->client->send_post_request(resource_dir_ip.c_str(),
                                        resource_dir_port.c_str(),
                                        "rd",
                                        query_string.c_str());
    }
}

int CoapServer::delete_resource(const char *uri) {
    // TODO: decouple deleting resource and un-sub to the resource directory
    coap_resource_t *to_delete = CoapServer::resources->get(uri);

    // delete resource locally
    if (to_delete == nullptr) {
        coap_log(LOG_WARNING, "Resource [%s] not found\n", uri);
        return EXIT_SUCCESS;
    }

    coap_delete_resource(this->context, to_delete);
    CoapServer::resources->erase(uri);

    return EXIT_SUCCESS;
}

// FIXME: finish implementation
void CoapServer::unpublish_resource(const char *uri) {
    // send the delete request to res_dir
    if (this->resource_dir_ip.empty()) {
        // res_dir unknown

        /*
         * send get multicast
         * get ip/port of res_dir
         * if null, start the voting
         * set ip/port of new res_dir
         *
         * send delete to res_dir
         */
    } else {
        // res_dir known
        this->client->send_delete_request(resource_dir_ip.c_str(),
                                          resource_dir_port.c_str(),
                                          uri);
    }
}

// ### RESOURCE DIR FUNCTIONS ###
void CoapServer::init_resource_dir_local() {
    coap_resource_t *r;

    r = coap_resource_init(coap_make_str_const(RD_ROOT_STR), 0);
    coap_register_handler(r, COAP_REQUEST_GET, CoapServer::hnd_get_rd);
    coap_register_handler(r, COAP_REQUEST_POST, CoapServer::hnd_post_rd);

    coap_add_attr(r, coap_make_str_const("ct"), coap_make_str_const("40"), 0);
    coap_add_attr(r, coap_make_str_const("rt"), coap_make_str_const("\"core.rd\""), 0);
    // TODO: check how `ins` is specified in the CoAP naming conventions
    coap_add_attr(r, coap_make_str_const("ins"), coap_make_str_const("\"default\""), 0);

    // TODO: potential multi-thread execution
    coap_add_resource(this->context, r);

    coap_resource_release_userdata_handler(this->context, CoapServer::resource_rd_delete);

    this->resource_dir_local = r;
}

// FIXME: make all handlers as private functions

void CoapServer::hnd_get_rd(coap_resource_t *resource COAP_UNUSED,
                            coap_session_t *session COAP_UNUSED,
                            const coap_pdu_t *request COAP_UNUSED,
                            const coap_string_t *query COAP_UNUSED,
                            coap_pdu_t *response) {
    // TODO: mutex for the get method?
    unsigned char buf[3];

    coap_pdu_set_code(response, COAP_RESPONSE_CODE_CONTENT);

    coap_add_option(response,
                    COAP_OPTION_CONTENT_TYPE,
                    coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_LINK_FORMAT),
                    buf);

    coap_add_option(response,
                    COAP_OPTION_MAXAGE,
                    coap_encode_var_safe(buf, sizeof(buf), 0x2ffff),
                    buf);
}

void CoapServer::hnd_post_rd(coap_resource_t *resource COAP_UNUSED,
                             coap_session_t *session,
                             const coap_pdu_t *request,
                             const coap_string_t *query,
                             coap_pdu_t *response) {

    if (!query) { // query is required
        coap_pdu_set_code(response, COAP_RESPONSE_CODE_BAD_OPTION);
        return;
    }

    // QUERY PARSING
    std::string query_str(reinterpret_cast<const char *>(query->s));
    auto sub_queries = split(query_str, '&');
    std::unordered_map<std::string, std::string> query_map;

    for (const auto &token: sub_queries) {
        auto sub_query = split(token, '=');
        query_map.insert(std::make_pair(sub_query[0], sub_query[1]));
    }

    // CHECK PRESENCE OF REQUIRED PARAMETERS
    {
        auto h = query_map.find("h");
        auto addr = query_map.find("addr");
        auto port = query_map.find("port");

        if (h == query_map.end() || addr == query_map.end() || port == query_map.end()) {
            coap_pdu_set_code(response, COAP_RESPONSE_CODE_BAD_OPTION);
            return;
        }
    }

    // CREATE THE NEW RESOURCE
    std::string resource_path = std::string(RD_ROOT_STR) + std::string("/") + query_map.find("h")->second;
    coap_resource_t *new_resource;
    new_resource = coap_resource_init(coap_make_str_const(resource_path.c_str()), 0);

    // ADD HANDLERS
    coap_register_handler(new_resource, COAP_REQUEST_GET, hnd_get_resource);
    coap_register_handler(new_resource, COAP_REQUEST_PUT, hnd_put_resource);
    coap_register_handler(new_resource, COAP_REQUEST_DELETE, hnd_delete_resource);

    // ADD RESOURCE ATTRIBUTES
    for (const auto &key: query_map) {
        std::string value = "\"" + key.second + "\"";

        coap_add_attr(new_resource,
                      coap_make_str_const(key.first.c_str()),
                      coap_make_str_const(value.c_str()),
                      0);
    }

    // ADD CREATED RESOURCE TO THE SERVER
    coap_add_resource(coap_session_get_context(session), new_resource);

    // AND TO THE RESOURCE MAP
    CoapServer::rd_resources->set(resource_path, new_resource);

    // SEND ANSWER TO CLIENT
    coap_pdu_set_code(response, COAP_RESPONSE_CODE_CREATED);

    {
        unsigned char _b[1024];
        unsigned char *b = _b;
        size_t buf_len = sizeof(_b);
        int n_seg;

        n_seg = coap_split_path(reinterpret_cast<const uint8_t *>(resource_path.c_str()),
                                resource_path.length(),
                                b,
                                &buf_len);

        while (n_seg--) {
            coap_add_option(response,
                            COAP_OPTION_LOCATION_PATH,
                            coap_opt_length(b),
                            coap_opt_value(b));
            b += coap_opt_size(b);
        }
    }
}

void CoapServer::hnd_get_resource(coap_resource_t *resource,
                                  coap_session_t *session COAP_UNUSED,
                                  const coap_pdu_t *request COAP_UNUSED,
                                  const coap_string_t *query COAP_UNUSED,
                                  coap_pdu_t *response) {
    coap_attr_t *etag;
    unsigned char buf[3];
    unsigned char print_buf[1024];
    auto *size = static_cast<size_t *>(malloc(sizeof(size_t)));
    *size = 1024;
    auto *offset = static_cast<size_t *>(malloc(sizeof(size_t)));
    *offset = 0;

    // TODO: what if the string is bigger than the buffer???

    auto uri = reinterpret_cast<const char *>(coap_resource_get_uri_path(resource)->s);
    auto resource_from_list = rd_resources->get(std::string(uri));

    coap_pdu_set_code(response, COAP_RESPONSE_CODE_CONTENT);
    coap_add_option(response,
                    COAP_OPTION_CONTENT_TYPE,
                    coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_APPLICATION_LINK_FORMAT),
                    buf);

    coap_print_link(resource_from_list, print_buf, size, offset);
    print_buf[*size] = '\0';

    etag = coap_find_attr(resource_from_list, coap_make_str_const("etag"));

    if (etag) {
        const char *value = (const char *) coap_attr_get_value(etag);
        if (strlen(value)) {
            coap_add_option(response,
                            COAP_OPTION_ETAG,
                            strlen(value),
                            (const uint8_t *) value);
        }
    }

    coap_add_data(response, *size, print_buf);
}

void CoapServer::hnd_put_resource(coap_resource_t *resource COAP_UNUSED,
                                  coap_session_t *session COAP_UNUSED,
                                  const coap_pdu_t *request COAP_UNUSED,
                                  const coap_string_t *query,
                                  coap_pdu_t *response) {
    // TODO: code to prevent modifications from unauthorized source
    const unsigned char *uri_path;
    coap_attr_t *resource_location;
    coap_address_t address{};
    std::string query_str((char *) query->s);
    std::stringstream string_stream;

    std::vector<std::string> tokens = split(query_str, '&');
    std::vector<std::vector<std::string>> query_a;
    query_a.reserve(tokens.size());

    for (const auto &a: tokens) {
        query_a.push_back(split(a, '='));
    }

    for (auto a: query_a) {
        const char *key = a[0].data();
        const char *value = a[1].data();

        coap_attr_t *attr = coap_find_attr(resource, coap_make_str_const(key));

        if (attr) {
            coap_attr_get_value(attr)->s = (const uint8_t *) value;
        } else {
            coap_add_attr(resource,
                          coap_make_str_const(key),
                          coap_make_str_const(value),
                          0);
        }
    }

    coap_pdu_set_code(response, COAP_RESPONSE_CODE_CHANGED);
}

void CoapServer::hnd_delete_resource(coap_resource_t *resource,
                                     coap_session_t *session,
                                     const coap_pdu_t *request,
                                     const coap_string_t *query,
                                     coap_pdu_t *response) {
    coap_context_t *context;
    int result;

    // TODO: possible multi-thread execution
    context = coap_session_get_context(session);
    result = coap_delete_resource(context, resource);
    auto res_name = std::string(reinterpret_cast<const char *>(coap_resource_get_uri_path(resource)->s));

    if (result) {
        rd_resources->erase(res_name);
        coap_pdu_set_code(response, COAP_RESPONSE_CODE_DELETED);
    } else {
        coap_pdu_set_code(response, COAP_RESPONSE_CODE_NOT_FOUND);
    }
}

void CoapServer::resource_rd_delete(void *ptr) {
    if (ptr) {
        coap_free(ptr);
    }
}


// ### SERVER FUNCTIONS ###
void CoapServer::resource_handler_get(coap_resource_t *resource,
                                      coap_session_t *session,
                                      const coap_pdu_t *request,
                                      const coap_string_t *query,
                                      coap_pdu_t *response) {
    unsigned char buf[3];

    std::string resource_name(reinterpret_cast<const char *>(coap_resource_get_uri_path(resource)->s));
    auto local_resource = resources->get(resource_name);
    auto coap_data = coap_resource_get_userdata(local_resource);

    coap_pdu_set_code(response, COAP_RESPONSE_CODE_CONTENT);
    coap_add_option(response,
                    COAP_OPTION_CONTENT_TYPE,
                    coap_encode_var_safe(buf, sizeof(buf), COAP_MEDIATYPE_TEXT_PLAIN),
                    buf);

    if (coap_data) {
        std::string resource_data(reinterpret_cast<const char *>(coap_data));

        coap_add_data(response,
                      resource_data.length(),
                      reinterpret_cast<const uint8_t *>(resource_data.c_str()));
    }
}

void CoapServer::resource_handler_put(coap_resource_t *resource,
                                      coap_session_t *session,
                                      const coap_pdu_t *request,
                                      const coap_string_t *query,
                                      coap_pdu_t *response) {
    std::string uri(reinterpret_cast<const char *>(coap_resource_get_uri_path(resource)->s));
    auto local_resource = resources->get(uri);
    auto resource_type_attr = coap_find_attr(local_resource, coap_make_str_const("rt"));

    std::string resource_type_str(reinterpret_cast<const char *>(coap_attr_get_value(resource_type_attr)));

    if (resource_type_str == "sensor") {
        coap_pdu_set_code(response, COAP_RESPONSE_CODE_NOT_ALLOWED);
        return;
    }

    auto *data_len = static_cast<size_t *>(malloc(sizeof(size_t)));
    const auto **data = static_cast<const uint8_t **>(malloc(sizeof(const uint8_t *)));
    coap_get_data(request, data_len, data);

    coap_resource_set_userdata(local_resource, (void *) *data);

    coap_pdu_set_code(response, COAP_RESPONSE_CODE_CHANGED);
}

void CoapServer::resource_handler_post(coap_resource_t *resource,
                                       coap_session_t *session,
                                       const coap_pdu_t *request,
                                       const coap_string_t *query,
                                       coap_pdu_t *response) {
    // FIXME: complete implementation
    coap_pdu_set_code(response, COAP_RESPONSE_CODE_UNAUTHORIZED);
}

void CoapServer::check_resource_expiration() {
    // TODO: requires a method to get all possible keys in the ThreadSafe.. methods

    /*
     * get current time
     * get last exec time
     *
     * while (list of resources) {
     *      update expiration
     *      if (remaining time <= 0) {
     *          remove resource
     *      }
     * }
     *
     * update last exec
     */

}

int CoapServer::run(/* pass a list of resources? */) {
    // TODO: create basic resources

    while (this->get_quit() == 0) {
        coap_io_process(this->context, COAP_IO_WAIT);
    }

    return 0;
}
